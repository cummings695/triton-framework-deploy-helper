﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml.Linq;
using DeployHelper.Logic;

namespace DeployHelper.View.Console
{
	class Program
	{
		static void Main(string[] args)
		{
			log4net.Config.XmlConfigurator.Configure(new FileInfo("log4net.console.config"));

			XDocument document = XDocument.Load(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),"fileconfig.xml"));

			if (args.Count() > 0) {
				TaskProcessor processor = new TaskProcessor(document);
				processor.Execute(args.ToList());
			} 
		}
	}
}