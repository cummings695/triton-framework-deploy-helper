﻿using System;
using System.Threading;
using DeployHelper.View.Forms;
using log4net.Appender;
using log4net.Core;

namespace DeployHelper.Logging
{
	public class DebugTextBoxAppender : AppenderSkeleton
	{
		protected override void Append(LoggingEvent loggingEvent)
		{
			if (MainForm.DebugTextBox != null) {
				try {
					MainForm.DebugTextBox.Text += RenderLoggingEvent(loggingEvent);
				} catch {}
			}
		}
	}
}