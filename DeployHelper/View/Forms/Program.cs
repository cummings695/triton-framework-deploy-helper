﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using System.Xml.Linq;

namespace DeployHelper.View.Forms
{
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main (string[] args)
		{
			log4net.Config.XmlConfigurator.Configure (new FileInfo ("log4net.forms.config"));

			XDocument document = XDocument.Load(args.Length > 0 ? args [0] : Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "fileconfig.xml"));

			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			Application.Run(new MainForm(document));
		}
	}
}